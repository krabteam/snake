// Snake.cpp: ���������� ����� ����� ��� ����������� ����������.
#pragma comment(lib, "user32")
#pragma comment(lib, "glaux.lib")

#include "stdafx.h"
using namespace std;

void LoadTextures();

unsigned int textures[1];

int snsp = 100;							// �������� ������: 50 - ������, 100 - ���������, 200 - ��������
int w1;									// ���������� ��� ���� ����������
int N = 25, M = 25;						// ������� ����
int Scale = 25;							// �������
int w = Scale*N;						// ������ ����
int h = Scale*M;						// ������ ����
int dir = -1, num = 4, mn = 1, ch = 2;	// dir - ����������� ������; 0 �����, 1 ������, 2 �������, 3 ����, -1 ������� ����
										// num - ���������� ��������� �� ������, ������� ��� 4
										// mn - ����� ���� (1 ���������, 2 ����� ��������, 3 ��������)
										// ch - �������� ������ (���������� ����������, �.�. 2)
struct
{
	int x;								// x,y - ���������� ������� ���������� ������
	int y;
	int type;							// ��� ��������: 0 - 3 ������ (����������� ��������� � dir), 4 ����
}  s[100];								// 100 - ������������ ���������� ����������� � ������

class Fructs
{
public:
	int x, y;

	void New()
	{
		int k = 0;
		x = rand() % N;
		y = rand() % M;
		for (int j = 0; j < num; j++)
		if ((s[j].x == x) && (s[j].y == y))
		{
			k = 1;							// ���� ������ ������� �� ������, �� �������� ����� ������, � � ������ ����������� �������
		}
		if (k == 1) New();
	}

	void DrawApple()						
	{
		glBegin(GL_QUADS);
			glTexCoord2f(0.125, 0.875); glVertex2f((m.x + 0.1)*Scale, (m.y + 0.1)*Scale);
			glTexCoord2f(0.125, 0.9375); glVertex2f((m.x + 0.1)*Scale, (m.y + 0.9)*Scale);
			glTexCoord2f(0.1875, 0.9375); glVertex2f((m.x + 0.9)*Scale, (m.y + 0.9)*Scale);
			glTexCoord2f(0.1875, 0.875); glVertex2f((m.x + 0.9)*Scale, (m.y + 0.1)*Scale);
		glEnd();
	}

} m;										// �������


void SnakeReset()
{
	s[0].x = 10; s[0].y = 10; s[0].type = 2;
	s[1].x = 9;  s[1].y = 10; s[1].type = 4;
	s[2].x = 8;  s[2].y = 10; s[2].type = 4;
	s[3].x = 7;  s[3].y = 10; s[3].type = 4;
	dir = 2;
}

void DrawSnake()
{
	for (int i = 1; i < num; i++)			// ������ glrectf ���������� ������� ������ �������; �������� 0 �� 1
	{
		glBegin(GL_QUADS);
			glTexCoord2f(0.0625, 0.875); glVertex2f((s[i].x)*Scale, (s[i].y)*Scale);
			glTexCoord2f(0.0625, 0.9375); glVertex2f((s[i].x)*Scale, (s[i].y+1)*Scale);
			glTexCoord2f(0.125, 0.9375); glVertex2f((s[i].x+1)*Scale, (s[i].y+1)*Scale);
			glTexCoord2f(0.125, 0.875); glVertex2f((s[i].x+1)*Scale, (s[i].y)*Scale);
		glEnd();
	}
	switch (s[0].type)
	{
	case 0:									// ������; ������ glrectf ���������� ������� ������ �������
		{ glBegin(GL_QUADS);
			glTexCoord2f(0.0, 0.9375); glVertex2f((s[0].x)*Scale, (s[0].y)*Scale);
			glTexCoord2f(0.0, 1.0); glVertex2f((s[0].x)*Scale, (s[0].y+1)*Scale);
			glTexCoord2f(0.0625, 1.0); glVertex2f((s[0].x+1)*Scale, (s[0].y+1)*Scale);
			glTexCoord2f(0.0625, 0.9375); glVertex2f((s[0].x+1)*Scale, (s[0].y)*Scale);
		glEnd();
		} break;
	case 1:									// ������; ������ glrectf ���������� ������� ������ �������
		{ glBegin(GL_QUADS);
			glTexCoord2f(0.0625, 0.9375); glVertex2f((s[0].x)*Scale, (s[0].y)*Scale);
			glTexCoord2f(0.0625, 1.0); glVertex2f((s[0].x)*Scale, (s[0].y+1)*Scale);
			glTexCoord2f(0.125, 1.0); glVertex2f((s[0].x+1)*Scale, (s[0].y+1)*Scale);
			glTexCoord2f(0.125, 0.9375); glVertex2f((s[0].x+1)*Scale, (s[0].y)*Scale);
		glEnd();
		} break;
	case 2:									// �������; ������ glrectf ���������� ������� ������ �������
		{ glBegin(GL_QUADS);
			glTexCoord2f(0.125, 0.9375); glVertex2f((s[0].x)*Scale, (s[0].y)*Scale);
			glTexCoord2f(0.125, 1.0); glVertex2f((s[0].x)*Scale, (s[0].y+1)*Scale);
			glTexCoord2f(0.1875, 1.0); glVertex2f((s[0].x+1)*Scale, (s[0].y+1)*Scale);
			glTexCoord2f(0.1875, 0.9375); glVertex2f((s[0].x+1)*Scale, (s[0].y)*Scale);
		glEnd();
		} break;
	case 3:									// ����; ������ glrectf ���������� ������� ������ �������
		{ glBegin(GL_QUADS);
			glTexCoord2f(0.0, 0.875); glVertex2f((s[0].x)*Scale, (s[0].y)*Scale);
			glTexCoord2f(0.0, 0.9375); glVertex2f((s[0].x)*Scale, (s[0].y+1)*Scale);
			glTexCoord2f(0.0625, 0.9375); glVertex2f((s[0].x+1)*Scale, (s[0].y+1)*Scale);
			glTexCoord2f(0.0625, 0.875); glVertex2f((s[0].x+1)*Scale, (s[0].y)*Scale);
		glEnd();
		} break;
	};
}

void Tick()									// ������� ���� ������
{
	int i;
	for (i = num; i > 0; --i)
	{
		s[i].x = s[i - 1].x;
		s[i].y = s[i - 1].y;				// �������� ���� ��������� ������, ����� ���������
	}

	if (dir == 0) s[0].y += 1;
	if (dir == 1) s[0].x -= 1;
	if (dir == 2) s[0].x += 1;
	if (dir == 3) s[0].y -= 1;
	if ((dir <= 3)&&(dir >= 0)) s[0].type = dir;					
	for (i = 1; i < num; i++) s[i].type = 4; // �������� ��������� �������� ������ � ����������� �� �������� �����������

	if ((s[0].x == m.x) && (s[0].y == m.y))
	{
		num++; m.New();						// ���� ������ ������� �� ������, �� �������� ����� ������, � � ������ ����������� �������
	}

	if (s[0].x >= N) { mn = 3; SnakeReset(); dir = -1; }
	if (s[0].x < 0)  { mn = 3; SnakeReset(); dir = -1; }
	if (s[0].y >= M) { mn = 3; SnakeReset(); dir = -1; }
	if (s[0].y < 0)  { mn = 3; SnakeReset(); dir = -1; }	// ���� ������ ���������� � ������, �� ��������

	for (int i = 1; i < num; i++)							// ���� ������ ������� �� ���� ����, �� �������� 
		if (s[0].x == s[i].x && s[0].y == s[i].y) { SnakeReset(); dir = -1; mn = 3; }
}

void DrawField()						// ������ ���� ��� ����
{
	glBegin(GL_QUADS);
		glTexCoord2f(0.5, 0.5); glVertex2f(0, 0);
		glTexCoord2f(0.5, 1.0); glVertex2f(0, h);
		glTexCoord2f(1.0, 1.0); glVertex2f(w, h);
		glTexCoord2f(1.0, 0.5); glVertex2f(w, 0);
	glEnd();
}

void DrawMenu()						// dir = -1
{
	if (mn == 1)
	{
		// Start (1)
		glBegin(GL_QUADS);
			glTexCoord2f(0.00390625, 0.734375); glVertex2f(5.0*Scale, 15.0*Scale);
			glTexCoord2f(0.00390625, 0.87109375); glVertex2f(5.0*Scale, 20.0*Scale);
			glTexCoord2f(0.5, 0.87109375); glVertex2f(20.0*Scale, 20.0*Scale);
			glTexCoord2f(0.5, 0.734375); glVertex2f(20.0*Scale, 15.0*Scale);
		glEnd();
		// Quit (ESC)
		glBegin(GL_QUADS);
			glTexCoord2f(0.00390625, 0.59375); glVertex2f(5.0*Scale, 5.0*Scale);
			glTexCoord2f(0.00390625, 0.734375); glVertex2f(5.0*Scale, 10.0*Scale);
			glTexCoord2f(0.5, 0.734375); glVertex2f(20.0*Scale, 10.0*Scale);
			glTexCoord2f(0.5, 0.59375); glVertex2f(20.0*Scale, 5.0*Scale);
		glEnd();
	}
	else {
		if (mn == 2)
		{

			// speed menu
			glBegin(GL_QUADS);
				glTexCoord2f(0.00390625, 0.14453125); glVertex2f(5.0*Scale, 5.0*Scale);
				glTexCoord2f(0.00390625, 0.5859375); glVertex2f(5.0*Scale, 20.0*Scale);
				glTexCoord2f(0.5, 0.5859375); glVertex2f(20.0*Scale, 20.0*Scale);
				glTexCoord2f(0.5, 0.14453125); glVertex2f(20.0*Scale, 5.0*Scale);
			glEnd();
		}
		else
			if (mn == 3)
			{

			// Repeat (1)
			glBegin(GL_QUADS);
				glTexCoord2f(0.00390625, 0.00390625); glVertex2f(5.0*Scale, 15.0*Scale);
				glTexCoord2f(0.00390625, 0.1327875); glVertex2f(5.0*Scale, 20.0*Scale);
				glTexCoord2f(0.5, 0.1327875); glVertex2f(20.0*Scale, 20.0*Scale);
				glTexCoord2f(0.5, 0.00390625); glVertex2f(20.0*Scale, 15.0*Scale);
			glEnd();
			// Quit (ESC)
			glBegin(GL_QUADS);
				glTexCoord2f(0.00390625, 0.59375); glVertex2f(5.0*Scale, 5.0*Scale);
				glTexCoord2f(0.00390625, 0.734375); glVertex2f(5.0*Scale, 10.0*Scale);
				glTexCoord2f(0.5, 0.734375); glVertex2f(20.0*Scale, 10.0*Scale);
				glTexCoord2f(0.5, 0.59375); glVertex2f(20.0*Scale, 5.0*Scale);
			glEnd();
			}
	}
}

void display() {

	glClear(GL_COLOR_BUFFER_BIT);		// ������� ������ �����

	if (dir != -1)
	{
		DrawField();
		DrawSnake();
		m.DrawApple();
	}
	else {
		num = 4;
		DrawMenu();
	}
	glFlush();							// �������, ��� ����� �����, ��� ���� � glbegin
	glutSwapBuffers();					// ����� ������ �.��������� �������� ����� �������, ��� �������� ��� �� �����, � � ������
}

void Finish()
{
	glutDestroyWindow(w1);				// ��������� ���� ����������
	_exit(EXIT_SUCCESS);				// ������������� ����� � ������ "������ ���������"
}

void KeyboardEvent(unsigned char key, int a, int b)
{
	switch (key)
	{
	// WASD/wasd keys
	case 'w': if ((dir != -1) || (dir != 3)) dir = 0; break;	// w, �����
	case 'd': if ((dir != -1) || (dir != 1)) dir = 2; break;	// d, �������
	case 'a': if ((dir != -1) || (dir != 2)) dir = 1; break;	// a, ������
	case 's': if ((dir != -1) || (dir != 0)) dir = 3; break;	// s, ����
	case 'W': if ((dir != -1) || (dir != 3)) dir = 0; break;	// W, �����
	case 'D': if ((dir != -1) || (dir != 1)) dir = 2; break;	// D, �������
	case 'A': if ((dir != -1) || (dir != 2)) dir = 1; break;	// A, ������
	case 'S': if ((dir != -1) || (dir != 0)) dir = 3; break;	// S, ����
	// ESC key
	case 27: Finish(); break;									// ESC, �����
	// 123 keys
	case 49: if (dir == -1) { if (mn != 2) mn = 2; else { ch = 1; snsp = 200; dir = 2;} } break;
																// 1, ���� ������/������ �����, �� ������ �������
																// ���� ������, �� ������ ��������
	case 50: if (dir == -1) { if (mn == 2) { ch = 2; snsp = 100; dir = 2;} } break;
																// 2, ������ �������� �� ������ ������
	case 51: if (dir == -1) { if (mn == 2) { ch = 3, snsp = 50; dir = 2;} } break;
																// 3, ������ �������� �� ������ ������
	}
}

void timer(int = 0)						// �������� ���� ���������
{
	display();							// ���������� ��

	if (dir != -1) Tick();				// �������, ��������� �� �������� �������, � ��� �������������� ��� ������

	glutTimerFunc(snsp, timer, 0);		// ����� SNSP �� ���������� �. Timer; ������������� ������� 0 (������ �������� ���)
}

int main(int argc, char **argv)
{
	srand(time(0));

	int i = 0;
	mn = 1;
	m.New();							// ������� ������

	SnakeReset(); dir = -1;				// ��������� ������� ������, � ��������� ����

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);	// ����������� ��������; RGB [0..1] (�� [0..255])
	glutInitWindowSize(w, h);			// �������� ���� �������� W*H
	w1 = glutCreateWindow("Snake by KRAB-TEAM");	//�������� ����, � ������� �� � ����������
	LoadTextures();
	glEnable(GL_TEXTURE_2D);
	glClearColor(0.0, 0.0, 0.0, 1.0);	// ������� ������� ����
	glMatrixMode(GL_PROJECTION);		// ����� ��� �������������, ��� ������� ������������
	glLoadIdentity();					// ��� ������� ���������� ���������
	gluOrtho2D(0, w, 0, h);				// ������� ��������������� �������� (�����, ������, ������, �������; ������� � ������ ��������
										// �������������, � ����� -1 � 1 ��������������)
	glMatrixMode(GL_MODELVIEW);
	glutDisplayFunc(display);			// ������������ ������� ������ �� �����
	glutKeyboardFunc(KeyboardEvent);	// ������������ ������� ������������� ������
	glutTimerFunc(snsp, timer, 0);		// ���������� ������� timer, SNSP �� ��������
	glutMainLoop();						// ����� � �������� ���� ���������
}
void LoadTextures()
{
	TCHAR buffer[MAX_PATH];
	GetCurrentDirectory(sizeof(buffer), buffer);
	_tcscat_s(buffer, "\\textures.bmp");
	AUX_RGBImageRec *texture1 = auxDIBImageLoad(buffer);
	glGenTextures(1, textures);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, texture1->sizeX, texture1->sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE, texture1->data);
}